﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace iBrain
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {InitializeComponent();}

        private void stepByStep_Click(object sender, RoutedEventArgs e)
        {
            Int32 level;
            if (easyRB.IsChecked == true) { level = 1; }
            else if (mediumRB.IsChecked == true) { level = 2; }
            else { level = 3; }

            StepByStepWindow ssw = new StepByStepWindow(level);
            ssw.ShowDialog();
        }

        private void findNumber_Click(object sender, RoutedEventArgs e)
        {
            Int32 level;
            if (easyRB.IsChecked == true) { level = 1; }
            else if (mediumRB.IsChecked == true) { level = 2; }
            else { level = 3; }

            FindTheNumberWindow ftnw = new FindTheNumberWindow(level);
            ftnw.ShowDialog();
        }

        private void rememberCells_Click(object sender, RoutedEventArgs e)
        {
            // Open the training #3(Remember the cells which was activated)

        }

        private void quickSort_Click(object sender, RoutedEventArgs e)
        {
            // ...
        }

        private void digitСomparison_Click(object sender, RoutedEventArgs e)
        {
            // ...
        }

        private void colorComparison_Click(object sender, RoutedEventArgs e)
        {
            // ...
        }

        private void dominateColors_Click(object sender, RoutedEventArgs e)
        {
            // ...
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
