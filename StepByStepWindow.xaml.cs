﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace iBrain
{
    public partial class StepByStepWindow : Window
    {
        Int32 currentCell = 1;
        Int32 _level;
        Button[] buttons = null; 
        Int32 countOfButtons;
        String currentTime = String.Empty;
        DispatcherTimer dt = new DispatcherTimer();
        Stopwatch sw = new Stopwatch();
        TextBlock tb = null;
        Button again = new Button();

        public StepByStepWindow(Int32 level)
        {
            InitializeComponent();
            _level = level;

            tb = new TextBlock();
            tb.Margin = new Thickness(220, -350, 0, 0);
            tb.FontSize = 25;
            sp.Children.Add(tb);
            dt.Tick += new EventHandler(dt_Tick);
            dt.Interval = new TimeSpan(0, 0, 0, 0, 1);

            again.Content = "Again";
            again.Height = 30; again.Width = 70;
            again.Margin = new Thickness(200, -590, 0, 0);
            sp.Children.Add(again);
            again.Click += Again_Click;

            Filling();
        }

        private void Filling()
        {
            if (_level == 1) { countOfButtons = 12; this.Height = 190; }
            else if (_level == 2) { countOfButtons = 20; this.Height = 290; }
            else { countOfButtons = 28; this.Height = 390; }
            buttons = new Button[countOfButtons];

            Random rand = new Random();
            Boolean flag = false;
            for (Int32 i = 0; i < countOfButtons; i++)
            {
                buttons[i] = new Button();
                buttons[i].Click += StepByStepWindow_Click;
                while (true)
                {
                    buttons[i].Content = rand.Next(1, countOfButtons + 1);
                    for (Int32 j = 0; j < i; j++)
                    {
                        if (String.Compare(buttons[i].Content.ToString(), buttons[j].Content.ToString()) == 0)
                        { flag = true; break; }
                    }
                    if (!flag) { break; }
                    flag = false;
                }
            }
            Painting();
        }
        private void Painting()
        {
            Int32 columns = 0;
            Int32 rows = 0;

            for (int i = 0; i < countOfButtons; i++)
            {
                ColumnDefinition cd = new ColumnDefinition();
                cd.Width = new GridLength(50, GridUnitType.Pixel);
                grid.ColumnDefinitions.Add(cd);

                RowDefinition rd = new RowDefinition();
                rd.Height = new GridLength(50, GridUnitType.Pixel);
                grid.RowDefinitions.Add(rd);

                Grid.SetColumn(buttons[i], columns);
                Grid.SetRow(buttons[i], rows);
                grid.Children.Add(buttons[i]);

                ++columns;
                if (columns == 4) { columns = 0; ++rows; }
            }
            sw.Start();
            dt.Start();
        }

        void dt_Tick(object sender, EventArgs e)
        {
            if (sw.IsRunning)
            {
                TimeSpan ts = sw.Elapsed;
                currentTime = String.Format("{0:00}:{1:00}:{2:00}",
                ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                tb.Text = currentTime;
            }
        }
        private void Again_Click(object sender, RoutedEventArgs e)
        {
            currentCell = 1;
            sw.Reset();
            tb.Text = "00:00:00";
            Filling();
        }
        private void StepByStepWindow_Click(object sender, RoutedEventArgs e)
        {
            if (currentCell == Convert.ToInt32(((Button)sender).Content))
            {
                ((Button)sender).Background = Brushes.LightGreen;
                if (currentCell == countOfButtons)
                {
                    sw.Stop();
                    dt.Stop();
                }
                else
                { ++currentCell; }
            }
            else
            {
                sw.Stop();
                dt.Stop();
                ((Button)sender).Background = Brushes.Red;
                MessageBox.Show("It's over :(");
                this.Close();
            }
        }
    }
}
