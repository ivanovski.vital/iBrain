﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace iBrain
{
    public partial class FindTheNumberWindow : Window
    {
        Int32 _level;
        Int32 rightAnswers = 0;
        Int32 timeLeft = 60;
        Button[] buttons = null;
        Int32 countOfButtons;
        TextBlock forTimer = null;
        TextBlock forDigit = null;
        DispatcherTimer dt = null;

        SolidColorBrush[] brushes = new SolidColorBrush[]
        {
            Brushes.Blue, Brushes.Green, Brushes.Red, Brushes.Yellow, Brushes.Violet
        };


        public FindTheNumberWindow(Int32 level)
        {
            InitializeComponent();
            _level = level;
            StartTimer();
            Filling();
        }

        private void StartTimer()
        {
            forTimer = new TextBlock();
            forTimer.FontSize = 18;
            forTimer.Foreground = Brushes.Red;
            forTimer.Margin = new Thickness(290, 0, 0, 0);
            sp2.Children.Add(forTimer);
            forTimer.Text = timeLeft.ToString();
            dt = new DispatcherTimer();
            dt.Tick += Dt_Tick;
            dt.Interval = new TimeSpan(0,0,0,0,1000);
            dt.Start();
        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            --timeLeft;

            if (timeLeft != 0)
            { forTimer.Text = timeLeft.ToString(); }
            else
            {
                grid.IsEnabled = false;
                dt.Stop();
                forTimer.Text = "0";
                forTimer.Foreground = Brushes.Green;

                MessageBox.Show(String.Format("The count of right answers: {0}", rightAnswers));
            }
        }

        private void Filling()
        {
            if (_level == 1) { countOfButtons = 12; this.Height = 190; }
            else if (_level == 2) { countOfButtons = 20; this.Height = 290; }
            else { countOfButtons = 28; this.Height = 390; }
            buttons = new Button[countOfButtons];

            Random rand = new Random();
            for (Int32 i = 0; i < countOfButtons; i++)
            {
                buttons[i] = new Button();
                buttons[i].Click += FindTheNumberWindow_Click;
                buttons[i].Content = rand.Next(1, 1000);
            }
            Painting();
        }

        private void Painting()
        {
            Int32 columns = 0;
            Int32 rows = 0;

            for (int i = 0; i < countOfButtons; i++)
            {
                ColumnDefinition cd = new ColumnDefinition();
                cd.Width = new GridLength(50, GridUnitType.Pixel);
                grid.ColumnDefinitions.Add(cd);

                RowDefinition rd = new RowDefinition();
                rd.Height = new GridLength(50, GridUnitType.Pixel);
                grid.RowDefinitions.Add(rd);

                Grid.SetColumn(buttons[i], columns);
                Grid.SetRow(buttons[i], rows);
                grid.Children.Add(buttons[i]);

                ++columns;
                if (columns == 4) { columns = 0; ++rows; }
            }

            Random rand = new Random();

            forDigit = new TextBlock();
            forDigit.FontSize = 35;
            forDigit.Margin = new Thickness(275, 30, 0, 0);
            sp2.Children.Add(forDigit);
            forDigit.Text = (buttons[rand.Next(0, countOfButtons)].Content).ToString();
        }

        private void FindTheNumberWindow_Click(object sender, RoutedEventArgs e)
        {
            if(String.Compare(((Button)sender).Content.ToString(), forDigit.Text) == 0)
            {
                forDigit.Text = "";
                ++rightAnswers;
                Filling();

                Random rand = new Random();
                for (int i = 0; i < countOfButtons; i++)
                {
                    buttons[i].Background = brushes[rand.Next(0, 5)];
                }
            }
        }
    }
}

